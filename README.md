# ministreamdeck

A mini stream-deck based on a Seeeduino board and MicroPython

Credits : [Build a simple USB HID Macropad using Seeeduino Xiao & CircuitPython](https://makeandymake.github.io/2020/05/02/seeeduino-xiao-circuitpython-usb-hid-macro-keypad.html) by [Andy Warburton](https://makeandymake.github.io/about/)

## Hardware

A [Seeduino XIAO](https://wiki.seeedstudio.com/Seeeduino-XIAO/)

8 keys are available

- 6 of the keys are mapped to <CTRL>-<ALT>-<SHIFT> F1 to F6
- The 2 remaining keys are mapped especialy for KeePass : <CTRL>-<ALT>-K and <CTRL>-<ALT>-A

## System integration

One may use AutoHotKey (Win) or [AutoKey](https://github.com/autokey/autokey) (Linux) to run scripts based on each typed key

- [AutoKey : Linux Utility for Text Substitution , Hotkeys and Desktop Automation](https://saravananthirumuruganathan.wordpress.com/2010/04/14/autokey-linux-utility-for-text-substitution-hotkeys-and-desktop-automation/)
## Keycodes

As the MicroPython librairy only support US (QWERTY) code, we have to send 'Q' to get 'A' on a french-keybpard (AZERTY)

https://circuitpython.readthedocs.io/projects/hid/en/latest/api.html

To send each of the keycode, we proceed in two step :

- __Step #1__ - We use kbd.press() to send key modifiers (CTRL, ALT, SHIFT, ....)
 ... Then we wait a little bit
- __Step #2__ - Afterwards, we type the key using kbd.send()

Doing so gave better results/detection than using a single kbd.press() with everything in it
