# Import the libraries
import time
import board
from digitalio import DigitalInOut, Direction, Pull
from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keycode import Keycode
from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS
import usb_hid

#
# A mini stream-deck based on a Seeeduino board and MicroPython
#
# Code inspiration : https://makeandymake.github.io/2020/05/02/seeeduino-xiao-circuitpython-usb-hid-macro-keypad.html
#
# 6 of the keys are mapped to <CTRL>-<ALT>-<SHIFT> F1 to F6
# The 2 remaining keys are mapped especialy for KeePass : <CTRL>-<ALT>-K and <CTRL>-<ALT>-A
#
# One may use AutoHotKey (Win) or AutoKey (Linux) to run scripts based on each typed key

#---------------------------------------------------
# LED
#---------------------------------------------------
# define output LED (onboard)
led = DigitalInOut(board.D13)
led.direction = Direction.OUTPUT
# define output LED (external)
led_ext = DigitalInOut(board.D8)
led_ext.direction = Direction.OUTPUT

# Little functions to blink both 2 LEDs
def leds_on():
    led.value = False
    led_ext.value = False
def leds_off():
    led.value = True
    led_ext.value = True

# flash the LED when booting
for x in range(0, 5):

	leds_on()
	time.sleep(0.2)
	leds_off()
	time.sleep(0.2)
        print("starting")

#---------------------------------------------------
# Keyboard
#---------------------------------------------------
# configure device as keyboard
kbd = Keyboard(usb_hid.devices)
layout = KeyboardLayoutUS(kbd)

# define buttons
d0 = DigitalInOut(board.D0)
d0.direction = Direction.INPUT
d0.pull = Pull.UP

d1 = DigitalInOut(board.D1) d1.direction = Direction.INPUT
d1.pull = Pull.UP

d2 = DigitalInOut(board.D2)
d2.direction = Direction.INPUT
d2.pull = Pull.UP

d3 = DigitalInOut(board.D3)
d3.direction = Direction.INPUT
d3.pull = Pull.UP

d4 = DigitalInOut(board.D4)
d4.direction = Direction.INPUT
d4.pull = Pull.UP

d5 = DigitalInOut(board.D5)
d5.direction = Direction.INPUT
d5.pull = Pull.UP

d6 = DigitalInOut(board.D6)
d6.direction = Direction.INPUT
d6.pull = Pull.UP

d7 = DigitalInOut(board.D7)
d7.direction = Direction.INPUT
d7.pull = Pull.UP

# See
# https://circuitpython.readthedocs.io/projects/hid/en/latest/api.html

# little function to open apps via spotlight - UNUSED CODE
def open_app(app):
	#kbd.send(Keycode.COMMAND, Keycode.SPACE)
	kbd.send(Keycode.WINDOWS)
	time.sleep(0.2)
	layout.write(app)
	time.sleep(0.2)
	#kbd.send(Keycode.ENTER)


# loop forever
while True:

    #-----------------------------------------------
    # 1 - We use kbd.press() to send key modifiers (CTRL, ALT, SHIFT, ....)
    # 2 - Then we wait a little bit
    # 3 - And afterwards, we type the key using kbd.send()
    # ... Doing so gave better results than using a single kbd.press() with everything in it
    #-----------------------------------------------
    if not d0.value:

    	# <CTRL>-<ALT>-<SHIFT> - F1
    	leds_on()
        kbd.press(Keycode.CONTROL, Keycode.ALT, Keycode.SHIFT)
        print("D0 PIN : <CTRL>-<ALT>-<SHIFT>-F1")
        time.sleep(0.1)
        kbd.send(Keycode.F1)
        kbd.release_all()
        time.sleep(0.5)  # debounce delay
        leds_off()

    if not d1.value:

    	# <CTRL>-<ALT>-<SHIFT> - F2
    	leds_on()
        kbd.press(Keycode.CONTROL, Keycode.ALT, Keycode.SHIFT)
        print("D1 PIN : <CTRL>-<ALT>-<SHIFT>-F2")
        time.sleep(0.1)
        kbd.send(Keycode.F2)
        kbd.release_all()
        time.sleep(0.5)  # debounce delay
        leds_off()

    if not d2.value:

    	# <CTRL>-<ALT>-<SHIFT> - F3
    	leds_on()
        kbd.press(Keycode.CONTROL, Keycode.ALT, Keycode.SHIFT)
        print("D2 PIN : <CTRL>-<ALT>-<SHIFT>-F3")
        time.sleep(0.1)
        kbd.send(Keycode.F3)
        kbd.release_all()
        time.sleep(0.5)  # debounce delay
        leds_off()

    if not d3.value:

    	# <CTRL>-<ALT>-<SHIFT> - F4
    	leds_on()
        print("D3 PIN : <CTRL>-<ALT>-<SHIFT>-F4")
        kbd.press(Keycode.CONTROL, Keycode.ALT, Keycode.SHIFT)
        time.sleep(0.1)
        kbd.send(Keycode.F4)
        kbd.release_all()
        time.sleep(0.5)  # debounce delay
    	leds_off()

    if not d4.value:

    	# <CTRL>-<ALT>-<SHIFT> - F5
    	leds_on()
        kbd.press(Keycode.CONTROL, Keycode.ALT, Keycode.SHIFT)
        print("D4 PIN : <CTRL>-<ALT>-<SHIFT>-F5")
        time.sleep(0.1)
        kbd.send(Keycode.F5)
        kbd.release_all()
        time.sleep(0.5)  # debounce delay
        leds_off()

    if not d5.value:

    	# <CTRL>-<ALT>-<SHIFT> - F6
    	leds_on()
        kbd.press(Keycode.CONTROL, Keycode.ALT, Keycode.SHIFT)
        print("D5 PIN : <CTRL>-<ALT>-<SHIFT>-F6")
        time.sleep(0.1)
        kbd.send(Keycode.F6)
        kbd.release_all()
        time.sleep(0.5)  # debounce delay
        leds_off()

    if not d6.value:

    	# <CTRL>-<ALT>-K
    	leds_on()
        kbd.press(Keycode.CONTROL, Keycode.ALT)
        print("D6 PIN : <CTRL>-<ALT>-K (Bring KeePass on top)")
        time.sleep(0.1)
        kbd.send(Keycode.K)
        kbd.release_all()
        time.sleep(0.5)  # debounce delay
        leds_off()

    if not d7.value:

    	# <CTRL>-<ALT>-A
    	leds_on()
        kbd.press(Keycode.CONTROL, Keycode.ALT)
        print("D7 PIN : <CTRL>-<ALT>-A (KeePass Auto-Type)")
        time.sleep(0.1)
        kbd.send(Keycode.Q)             ## WARNING: We send 'Q' but 'A' will be typed on an AZERTY keyboard
        kbd.release_all()
        time.sleep(0.5)  # debounce delay
        leds_off()
